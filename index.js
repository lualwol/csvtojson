const chalk = require("chalk");
const fs = require("fs");
const csv = require("csv-parser");
const format = require("xml-formatter");
const glob = require("glob");

const log = console.log;

// log(chalk.red('Hello', chalk.underline.bgBlue('world') + '!'));
log(chalk.whiteBright("————————————Starting Parser————————————"));

glob("csv/*.csv", function (er, files) {
  // files is an array of filenames.
  if (er) {
    log(chalk.red("there was an error", er));
  }
  if (files.length != 0) {
    log(chalk.blue(chalk.yellow("Found CSV's :"), files));
    readCSV(files);
  } else {
    log(chalk.yellow("Did not find any CSV's in the csv folder "), files);
  }
  // If the `nonull` option is set, and nothing
  // was found, then files is ["**/*.js"]
  // er is an error object or null.
});

function readCSV(paths) {
  // (header === '') ? isHeaderEmpty++ : ''
  paths.forEach((path) => {
    let json = [];
    let isHeaderEmpty = 0;

    fs.createReadStream(path)
      .pipe(csv())
      .on("headers", (headers) => {
        headers.forEach((header) => {
          header === "" ? isHeaderEmpty++ : "";
        });
        // log(chalk.red("————————————————"))
      })
      .on("data", (data) => json.push(data))
      .on("end", () => {
        if (isHeaderEmpty) {
          log(
            chalk.red(
              `Found ${isHeaderEmpty} Empty Header${
                isHeaderEmpty == 1 ? "" : "s"
              }. Please Fix: ${path}`
            )
          );
        } else {
          writeJSONFile(json, path);
        }
        json.length = 0;
      });
  });
}

function writeJSONFile(jsonData, path) {
  // console.log(path)
  const purePath = path.slice(4, path.length - 4);
  const JSONPath = `output/${purePath}.json`;
  const JSONString = JSON.stringify(jsonData, null, 4);

  fs.writeFile(JSONPath, JSONString, (err) => {
    console.log("JSON file successfully written!");
  });
}
